package com.catalog.catalogmanagementsystem.controller;

import com.catalog.catalogmanagementsystem.category.CategoryEntity;
import com.catalog.catalogmanagementsystem.department.DepartmentEntity;
import com.catalog.catalogmanagementsystem.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/departments")

public class DepartmentController {

    @Autowired
    private DepartmentRepository departmentRepository;

    @PostMapping()
    public ResponseEntity<Object> saveItem(@RequestBody DepartmentEntity department) {

        DepartmentEntity savedItem = departmentRepository.save(department);

        if (savedItem != null) {

            return ResponseEntity.status(HttpStatus.CREATED).body(savedItem);
        } else {

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping()

    public ResponseEntity<List<DepartmentEntity>> getDepartments() {


        List<DepartmentEntity> departmentDetailList = new ArrayList<>();
        departmentDetailList = departmentRepository.findAll();

        if ( departmentDetailList!= null) {
            return ResponseEntity.status(HttpStatus.OK).body(departmentDetailList);

        } else {

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }


    }
}
