package com.catalog.catalogmanagementsystem.controller;

import com.catalog.catalogmanagementsystem.item.ItemEntity;
import com.catalog.catalogmanagementsystem.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/items")
public class ItemController {


    @Autowired
    private ItemRepository itemRepository;




    @PostMapping()
    public ResponseEntity<Object> saveItem(@RequestBody ItemEntity item) {

        ItemEntity  savedItem = itemRepository.save(item);

        if (savedItem != null) {

            return ResponseEntity.status(HttpStatus.CREATED).body(savedItem);
        } else {

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

    }


    @GetMapping()

    public ResponseEntity<List<ItemEntity>> getItems() {


        List<ItemEntity> itemList = new ArrayList<>();
        itemList = itemRepository.findAll();

        if (itemList != null) {
            return ResponseEntity.status(HttpStatus.OK).body(itemList);

        } else {

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

    }





}
