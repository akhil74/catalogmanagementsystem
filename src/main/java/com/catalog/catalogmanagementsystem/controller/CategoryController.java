package com.catalog.catalogmanagementsystem.controller;


import com.catalog.catalogmanagementsystem.category.CategoryEntity;
import com.catalog.catalogmanagementsystem.item.ItemEntity;
import com.catalog.catalogmanagementsystem.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/categories")
public class CategoryController {


    @Autowired
    private CategoryRepository categoryRepository;

    @PostMapping()
    public ResponseEntity<Object> saveItem(@RequestBody CategoryEntity category) {

        CategoryEntity savedItem = categoryRepository.save(category);

        if (savedItem != null) {

            return ResponseEntity.status(HttpStatus.CREATED).body(savedItem);
        } else {

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping()

    public ResponseEntity<List<CategoryEntity>> getCategories() {


        List<CategoryEntity> categoryDetailList = new ArrayList<>();
        categoryDetailList = categoryRepository.findAll();

        if (categoryDetailList != null) {
            return ResponseEntity.status(HttpStatus.OK).body(categoryDetailList);

        } else {

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }


    }
}
