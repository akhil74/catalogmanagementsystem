package com.catalog.catalogmanagementsystem.item;

import com.catalog.catalogmanagementsystem.category.CategoryEntity;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "item")
public class ItemEntity {

    @Id
    @Column(name="ItemId")
    private String itemId;
    @Column(name="ItemName")
    private String itemName;
    @Column(name="Upc")
    private String upc;
    @Column(name = "CostPrice")
    private BigDecimal costPrice;
    @Column(name="SellingPrice")
    private BigDecimal sellingPrice;
    @Column(name="DepartmentId")
    private Long departmentId;
    @Column(name = "CategoryId")
    private Long categoryId;
    @Column(name="Status")
    private String status;


    public ItemEntity() {
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getUpc() {
        return upc;
    }

    public void setUpc(String upc) {
        this.upc = upc;
    }

    public BigDecimal getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(BigDecimal costPrice) {
        this.costPrice = costPrice;
    }

    public BigDecimal getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(BigDecimal sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
