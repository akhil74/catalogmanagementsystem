package com.catalog.catalogmanagementsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CatalogmanagementsystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(CatalogmanagementsystemApplication.class, args);
	}



}
