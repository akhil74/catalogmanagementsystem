package com.catalog.catalogmanagementsystem.repository;

import com.catalog.catalogmanagementsystem.department.DepartmentEntity;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DepartmentRepository extends JpaRepository<DepartmentEntity,Integer> {




}
