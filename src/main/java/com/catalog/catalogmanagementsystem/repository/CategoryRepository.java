package com.catalog.catalogmanagementsystem.repository;

import com.catalog.catalogmanagementsystem.category.CategoryEntity;
import com.catalog.catalogmanagementsystem.department.DepartmentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<CategoryEntity,Integer> {

}
