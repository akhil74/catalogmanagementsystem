package com.catalog.catalogmanagementsystem.repository;

import com.catalog.catalogmanagementsystem.item.ItemEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemRepository extends JpaRepository<ItemEntity,String> {



}
